/*Dependencies*/
const express = require("express");
const router = express.Router();

/*File directories*/
const auth = require("../auth")
const checkoutControllers = require("../controllers/checkoutControllers");


/*Change status of orders*/
// router.post("/checkout/:cartItemId", auth.tokenVerification, checkoutControllers.checkout);

/*Grand total for all the items*/
// router.get("/viewTotal", auth.tokenVerification, checkoutControllers.checkoutAll);


module.exports = router; 