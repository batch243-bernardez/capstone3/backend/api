/*Dependencies*/
const mongoose = require("mongoose");


const checkoutSchema = new mongoose.Schema({

	cartItemId: {
		type: String,
		required: [true, "Cart is required!"]
	},

	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	customerName: {
		type: String,
		required: [true, "Customer name is required!"]
	},

	productId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	productName: {
		type: String,
		required: [true, "Product name is required!"]
	},

	productPrice: {
		type: Number,
		required: [true, "Product price is required!"]
	},

	quantity: {
		type: Number,
		required: [true, "Quantity is required!"]
	},

	subtotal: {
		type: Number,
		required: [true, "Total amount is required!"]
	},

	status: {
		type: String,
		default: "Checked out"
	}
	
}, {timestamps: true})

module.exports = mongoose.model("Checkout", checkoutSchema);




/*const mongoose = require("mongoose");

const checkoutSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	deliveryAddress: {
		type: String,
		required: [true, "Delivery address is required!"]
	},*/

	/*deliveryFee: {
		type: Number,
		default: 50
	},*/

	/*productId: {
		type: String,
		required: [true, "Product ID is required!"]
	},

	totalQuantity: {
		type: Number,
		required: [true, "Total quantity is required!"]
	},

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required!"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Checkout", checkoutSchema);*/

/*paymentMode: {
		type: String,
		required: [true, "Payment mode is required!"]
	},*/