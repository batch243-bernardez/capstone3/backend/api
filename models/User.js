const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required!"]
	},

	lastName: {
		type: String,
		required: [true, "Last name is required!"]
	},

	userName: {
		type: String,
		required: [true, "Username is required!"]
	},

	email: {
		type: String,
		required: [true, "Email is required!"]
	},

	password: {
		type: String,
		required: [true, "Password is required!"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile number is required!"]
	},

	order: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Orders'
	}],

	isAdmin: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model("User", userSchema);