const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	category: {
		type: String,
		required: [true, "Product category is required!"]
	},

	image: {
		type: String,
		required: [true, "Product image is required!"]
	},

	name: {
		type: String,
		required: [true, "Product name is required!"]		
	},

	description: {
		type: String,
		required: [true, "Product description is required!"]
	},

	price: {
		type: Number,
		required: [true, "Product price is required!"]
	},

	stocks: {
		type: Number,
		required: [true, "Stocks is required!"]
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	isActive: {
		type: Boolean,
		default: true
	}
});

module.exports = mongoose.model("Products", productSchema);